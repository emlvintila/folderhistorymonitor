package com.emanuel.FolderHistoryMonitor;

public class Constants {
    private Constants() {
    }

    public static final String IGNORE_FILE_NAME = ".fhmignore";
    public static final String DB_FILE_NAME = ".fhmdb";
}
