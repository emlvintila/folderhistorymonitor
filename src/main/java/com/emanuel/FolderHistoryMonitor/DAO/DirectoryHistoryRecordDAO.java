package com.emanuel.FolderHistoryMonitor.DAO;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "DirectoryHistoryRecord")
public class DirectoryHistoryRecordDAO {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    private FileHistoryRecordDAO fileHistoryRecord;

    private String eventKind;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public FileHistoryRecordDAO getFileHistoryRecord() {
        return fileHistoryRecord;
    }

    public void setFileHistoryRecord(FileHistoryRecordDAO fileHistoryRecord) {
        this.fileHistoryRecord = fileHistoryRecord;
    }

    public String getEventKind() {
        return eventKind;
    }

    public void setEventKind(String eventKind) {
        this.eventKind = eventKind;
    }
}
