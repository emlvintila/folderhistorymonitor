package com.emanuel.FolderHistoryMonitor.DAO;

import javax.persistence.Embeddable;

@Embeddable
public class FileHistoryRecordDAO {
    private String path;
    private long size;
    private long timestamp;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
