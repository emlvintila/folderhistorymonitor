package com.emanuel.FolderHistoryMonitor;

import java.nio.file.WatchEvent;

public class DirectoryHistoryRecord {
    private final FileHistoryRecord fileHistoryRecord;

    private final WatchEvent.Kind<?> eventKind;

    public DirectoryHistoryRecord(FileHistoryRecord fileHistoryRecord, WatchEvent.Kind<?> eventKind) {
        this.fileHistoryRecord = fileHistoryRecord;
        this.eventKind = eventKind;
    }

    public FileHistoryRecord getFileHistoryRecord() {
        return fileHistoryRecord;
    }

    public WatchEvent.Kind<?> getEventKind() {
        return eventKind;
    }

    @Override
    public String toString() {
        return "DirectoryHistoryRecord{" +
            "fileHistoryRecord=" + fileHistoryRecord +
            ", eventKind=" + eventKind +
            '}';
    }
}
