package com.emanuel.FolderHistoryMonitor;

import com.emanuel.FolderHistoryMonitor.Services.DirectoryHistoryService;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.*;
import java.util.function.Consumer;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class DirectoryMonitor {
    private final Path directory;

    private final DirectoryHistoryService directoryService;

    private final WatchService watchService;

    private final DirectoryRecord directoryRecord;

    /**
     * A list of regular expressions that will be tested for each file. If any of them match,
     * then the file is ignored.
     */
    private final List<String> ignoredFilesRegexes;

    private final List<Consumer<FileHistoryRecord>> listeners = new ArrayList<>();

    public DirectoryMonitor(Path directory, DirectoryHistoryService directoryService) throws IOException {
        this.directory = directory.toAbsolutePath();
        this.directoryService = directoryService;
        watchService = FileSystems.getDefault().newWatchService();
        directoryRecord = new DirectoryRecord(this.directory);

        List<DirectoryHistoryRecord> updateRecords = this.directoryService.getAllRecords().collect(Collectors.toList());
        directoryRecord.addAllRecords(updateRecords);

        List<String> ignoredFiles = new ArrayList<>();
        try {
            List<String> ignoreFileRules =
                Utils.parseIgnoreFile(this.directory.resolve(Constants.IGNORE_FILE_NAME)).collect(Collectors.toList());
            ignoredFiles.addAll(ignoreFileRules);
        } catch (IOException exception) {
            // can't read .fhmignore file
        }
        // always ignore the database-related files
        String dbFilesIgnorePattern = Pattern.quote(Constants.DB_FILE_NAME) + ".*";
        ignoredFiles.add(dbFilesIgnorePattern);
        ignoredFilesRegexes = Collections.unmodifiableList(ignoredFiles);

        directory.register(watchService, StandardWatchEventKinds.ENTRY_CREATE,
            StandardWatchEventKinds.ENTRY_MODIFY, StandardWatchEventKinds.ENTRY_DELETE);
    }

    public Path getDirectory() {
        return directory;
    }

    public DirectoryRecord getDirectoryRecord() {
        return directoryRecord;
    }

    public void startMonitoring() {
        while (true) {
            WatchKey key;
            try {
                key = watchService.take();
            } catch (InterruptedException e) {
                // stop monitoring if the thread has been interrupted
                break;
            }

            List<WatchEvent<?>> watchEvents = key.pollEvents();
            for (WatchEvent<?> event : watchEvents) {
                if (!isValidEvent(event)) {
                    continue;
                }

                WatchEvent<Path> pathEvent = eventCast(event);
                Path filename = pathEvent.context();
                if (!isValidFilename(filename)) {
                    continue;
                }

                FileHistoryRecord fileHistoryRecord = makeFileRecordForEvent(pathEvent);
                DirectoryHistoryRecord directoryHistoryRecord = new DirectoryHistoryRecord(fileHistoryRecord, event.kind());

                directoryService.insertRecord(directoryHistoryRecord);
                directoryRecord.addRecord(directoryHistoryRecord);

                listeners.forEach(listener -> listener.accept(fileHistoryRecord));
            }

            if (!key.reset()) {
                break;
            }
        }
    }

    public void addListener(Consumer<FileHistoryRecord> consumer) {
        listeners.add(consumer);
    }

    public void removeListener(Consumer<FileHistoryRecord> consumer) {
        listeners.remove(consumer);
    }

    @SuppressWarnings("unchecked")
    private WatchEvent<Path> eventCast(WatchEvent<?> event) {
        return (WatchEvent<Path>) event;
    }

    private boolean isValidEvent(WatchEvent<?> event) {
        return event.kind() != StandardWatchEventKinds.OVERFLOW;
    }

    private boolean isValidFilename(Path filename) {
        String filenameString = filename.toString();

        return ignoredFilesRegexes.stream().noneMatch(filenameString::matches);
    }

    private FileHistoryRecord makeFileRecordForEvent(WatchEvent<Path> event) {
        Path filename = event.context();
        Path file = directory.resolve(filename);

        // file modification time is (usually) the same as file creation time for newly created files
        if (event.kind() == StandardWatchEventKinds.ENTRY_CREATE ||
            event.kind() == StandardWatchEventKinds.ENTRY_MODIFY) {
            return makeFileModifiedRecord(file);
        }

        if (event.kind() == StandardWatchEventKinds.ENTRY_DELETE) {
            return makeFileDeletedRecord(file);
        }

        throw new IllegalArgumentException("Invalid event with kind " + event.kind());
    }

    private FileHistoryRecord makeFileModifiedRecord(Path file) {
        Optional<BasicFileAttributes> attributes = Utils.getBasicFileAttributes(file);
        Long size = attributes.map(BasicFileAttributes::size)
            .orElse(-1L);
        Long modificationTime = attributes.map(BasicFileAttributes::lastModifiedTime).map(FileTime::toMillis)
            .orElse(-1L);

        return new FileHistoryRecord(file.getFileName().toString(), size, modificationTime);
    }

    private FileHistoryRecord makeFileDeletedRecord(Path file) {
        return new FileHistoryRecord(file.getFileName().toString(), -1L, new Date().toInstant().toEpochMilli());
    }
}
