package com.emanuel.FolderHistoryMonitor;

import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DirectoryRecord {
    private final Path path;

    private final Map<String, List<DirectoryHistoryRecord>> updateRecords = new HashMap<>();

    public DirectoryRecord(Path path) {
        this.path = path;
    }

    public Path getPath() {
        return path;
    }

    public void addRecord(DirectoryHistoryRecord record) {
        addAllRecords(List.of(record));
    }

    public void addAllRecords(Collection<? extends DirectoryHistoryRecord> records) {
        records.stream()
            .collect(Collectors.groupingBy(this::getRecordAbsolutePath))
            .forEach((key, list) -> updateRecords.merge(key, new ArrayList<>(list),
                (left, right) -> Stream.concat(left.stream(), right.stream()).collect(Collectors.toUnmodifiableList()))
            );
    }

    public Collection<DirectoryHistoryRecord> getRecords(Path file) {
        return updateRecords.getOrDefault(path.resolve(file.getFileName()).toString(), new ArrayList<>());
    }

    private String getRecordAbsolutePath(DirectoryHistoryRecord record) {
        return path.resolve(record.getFileHistoryRecord().getPath()).toString();
    }
}
