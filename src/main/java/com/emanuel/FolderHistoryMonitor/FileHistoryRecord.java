package com.emanuel.FolderHistoryMonitor;

import java.nio.file.Path;
import java.util.Optional;

public class FileHistoryRecord {
    /**
     * Relative path to the containing <code>DirectoryRecord</code>
     */
    private final String path;
    /**
     * File size in bytes
     */
    private final long size;
    /**
     * Timestamp as ticks since unix epoch time
     */
    private final long timestamp;

    public FileHistoryRecord(String path, long size, long timestamp) {
        this.path = Path.of(path).getFileName().toString();
        this.size = size;
        this.timestamp = timestamp;
    }

    public String getPath() {
        return path;
    }

    public Optional<String> getExtension() {
        String[] parts = path.split("\\.");
        if (parts.length == 1)
            return Optional.empty();

        return Optional.of(parts[parts.length - 1]);
    }

    public long getSize() {
        return size;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return "FileHistoryRecord{" +
            "path='" + path + '\'' +
            ", size=" + size +
            ", timestamp=" + timestamp +
            '}';
    }
}
