package com.emanuel.FolderHistoryMonitor;

import com.emanuel.FolderHistoryMonitor.DAO.DirectoryHistoryRecordDAO;
import com.emanuel.FolderHistoryMonitor.Services.DirectoryHistoryService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainClass {
    public static void main(String[] args) throws IOException {
        Optional<String> watchedDirectoryPathString = Arrays.stream(args).findFirst();
        if (watchedDirectoryPathString.isEmpty()) {
            System.out.println("Please specify a path to a directory on the command line.");
            return;
        }

        Optional<Path> watchedDirectoryPath = watchedDirectoryPathString.map(Path::of).filter(Files::isDirectory);
        if (watchedDirectoryPath.isEmpty()) {
            System.out.printf("The specified path \"%s\" is invalid.\n", watchedDirectoryPathString.get());
            return;
        }

        Path directory = watchedDirectoryPath.get().toAbsolutePath();
        createAndStartMonitor(directory);

        System.out.printf("Watching directory \"%s\"\nPress ^C to exit.\n", directory.toString());
    }

    public static DirectoryMonitor createAndStartMonitor(Path directory) throws IOException {
        String databasePath = directory.resolve(Constants.DB_FILE_NAME).toString();
        String connectionString = "jdbc:hsqldb:file:" + databasePath;
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
            .configure()
            .applySetting("hibernate.connection.url", connectionString)
            .build();

        SessionFactory sessionFactory;
        try {
            sessionFactory = new MetadataSources(registry)
                .addAnnotatedClass(DirectoryHistoryRecordDAO.class)
                .buildMetadata()
                .buildSessionFactory();
        } catch (Exception e) {
            StandardServiceRegistryBuilder.destroy(registry);
            throw e;
        }

        ExecutorService executor = Executors.newSingleThreadExecutor();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            // gracefully shutdown database
            try (Session session = sessionFactory.openSession()) {
                session.beginTransaction();
                session.createNativeQuery("SHUTDOWN").executeUpdate();
                session.flush();
                session.getTransaction().commit();
            }
            executor.shutdownNow();
        }));

        DirectoryHistoryService service = new DirectoryHistoryService(sessionFactory);
        DirectoryMonitor monitor = new DirectoryMonitor(directory, service);
        executor.submit(monitor::startMonitoring);

        return monitor;
    }
}
