package com.emanuel.FolderHistoryMonitor.Services;

import com.emanuel.FolderHistoryMonitor.DAO.DirectoryHistoryRecordDAO;
import com.emanuel.FolderHistoryMonitor.DAO.FileHistoryRecordDAO;
import com.emanuel.FolderHistoryMonitor.DirectoryHistoryRecord;
import com.emanuel.FolderHistoryMonitor.FileHistoryRecord;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.util.stream.Stream;

public class DirectoryHistoryService {
    private final org.hibernate.SessionFactory SessionFactory;

    public DirectoryHistoryService(SessionFactory sessionFactory) {
        SessionFactory = sessionFactory;
    }

    public Stream<DirectoryHistoryRecord> getAllRecords() {
        try (Session session = SessionFactory.openSession()) {
            return session
                .createQuery("from DirectoryHistoryRecord", DirectoryHistoryRecordDAO.class)
                .getResultList()
                .stream()
                .map(dao ->
                    new DirectoryHistoryRecord(
                        mapFileRecordDAO(dao.getFileHistoryRecord()),
                        mapEventString(dao.getEventKind())
                    )
                );
        }
    }

    public void insertRecord(DirectoryHistoryRecord record) {
        DirectoryHistoryRecordDAO dao = mapDirectoryRecord(record);

        try (Session session = SessionFactory.openSession()) {
            session.beginTransaction();
            session.save(dao);
            session.flush();
            session.getTransaction().commit();
        }
    }

    private FileHistoryRecord mapFileRecordDAO(FileHistoryRecordDAO fileRecord) {
        return new FileHistoryRecord(fileRecord.getPath(), fileRecord.getSize(), fileRecord.getTimestamp());
    }

    private FileHistoryRecordDAO mapFileRecord(FileHistoryRecord fileHistoryRecord) {
        FileHistoryRecordDAO dao = new FileHistoryRecordDAO();
        dao.setPath(fileHistoryRecord.getPath());
        dao.setSize(fileHistoryRecord.getSize());
        dao.setTimestamp(fileHistoryRecord.getTimestamp());

        return dao;
    }

    private DirectoryHistoryRecordDAO mapDirectoryRecord(DirectoryHistoryRecord directoryRecord) {
        DirectoryHistoryRecordDAO dao = new DirectoryHistoryRecordDAO();
        dao.setFileHistoryRecord(mapFileRecord(directoryRecord.getFileHistoryRecord()));
        dao.setEventKind(mapEventKind(directoryRecord.getEventKind()));

        return dao;
    }

    private WatchEvent.Kind<?> mapEventString(String eventString) {
        switch (eventString) {
            case "ENTRY_CREATE":
                return StandardWatchEventKinds.ENTRY_CREATE;
            case "ENTRY_MODIFY":
                return StandardWatchEventKinds.ENTRY_MODIFY;
            case "ENTRY_DELETE":
                return StandardWatchEventKinds.ENTRY_DELETE;
            default:
                throw new IllegalArgumentException("Unexpected value: " + eventString);
        }
    }

    private String mapEventKind(WatchEvent.Kind<?> eventKind) {
        if (eventKind == StandardWatchEventKinds.ENTRY_CREATE)
            return "ENTRY_CREATE";

        if (eventKind == StandardWatchEventKinds.ENTRY_MODIFY)
            return "ENTRY_MODIFY";

        if (eventKind == StandardWatchEventKinds.ENTRY_DELETE)
            return "ENTRY_DELETE";

        throw new IllegalArgumentException("Unexpected value: " + eventKind);
    }
}
