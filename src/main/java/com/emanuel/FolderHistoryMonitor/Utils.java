package com.emanuel.FolderHistoryMonitor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributeView;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Optional;
import java.util.stream.Stream;

public class Utils {
    private Utils() {
    }

    public static Stream<String> parseIgnoreFile(Path file) throws IOException {
        return Files.readAllLines(file)
            .stream()
            .map(line -> line.replaceFirst("#.*", ""))
            .filter(String::isBlank);
    }

    public static Optional<BasicFileAttributes> getBasicFileAttributes(Path file) {
        try {
            BasicFileAttributes fileAttributes =
                Files.getFileAttributeView(file, BasicFileAttributeView.class).readAttributes();

            return Optional.of(fileAttributes);

        } catch (IOException exception) {
            // can't retrieve attributes (e.g. file doesn't exist)
            return Optional.empty();
        }
    }
}
