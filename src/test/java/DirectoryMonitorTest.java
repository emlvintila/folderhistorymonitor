import com.emanuel.FolderHistoryMonitor.DirectoryMonitor;
import com.emanuel.FolderHistoryMonitor.DirectoryRecord;
import com.emanuel.FolderHistoryMonitor.MainClass;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;

public class DirectoryMonitorTest {
    private Path directory;
    private DirectoryMonitor monitor;
    private DirectoryRecord record;

    @Before
    public void setUp() throws IOException {
        directory = Files.createTempDirectory(null).toAbsolutePath();
        monitor = MainClass.createAndStartMonitor(directory);
        record = monitor.getDirectoryRecord();
    }

    @After
    public void tearDown() {
        directory = null;
        monitor = null;
        record = null;
    }

    @Test
    public void CreateFiles() throws IOException, InterruptedException {
        Path file = Files.createTempFile(directory, null, null);

        Thread.sleep(1000);

        Assert.assertTrue("Created file is not present in directory record.",
            record.getRecords(file)
                .stream()
                .anyMatch(r -> r.getFileHistoryRecord().getPath().equals(file.getFileName().toString()))
        );

        Assert.assertTrue("There is no ENTRY_CREATE record for the file.",
            record.getRecords(file)
                .stream()
                .anyMatch(r -> r.getEventKind() == StandardWatchEventKinds.ENTRY_CREATE)
        );
    }

    @Test
    public void ModifyFiles() throws IOException, InterruptedException {
        Path file = Files.createTempFile(directory, null, null);
        Files.writeString(file, file.toString());

        Thread.sleep(1000);

        Assert.assertTrue("There is no ENTRY_MODIFY record for the file.",
            record.getRecords(file)
                .stream()
                .anyMatch(r -> r.getEventKind() == StandardWatchEventKinds.ENTRY_MODIFY)
        );
    }

    @Test
    public void DeleteFiles() throws IOException, InterruptedException {
        Path file = Files.createTempFile(directory, null, null);
        Files.delete(file);

        Thread.sleep(1000);

        Assert.assertTrue("There is no ENTRY_DELETE record for the file.",
            record.getRecords(file)
                .stream()
                .anyMatch(r -> r.getEventKind() == StandardWatchEventKinds.ENTRY_DELETE)
        );
    }
}
