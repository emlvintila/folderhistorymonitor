import com.emanuel.FolderHistoryMonitor.FileHistoryRecord;
import org.junit.Assert;
import org.junit.Test;

public class FileHistoryRecordTest {
    @Test
    public void ExtensionMissing() {
        var record = new FileHistoryRecord("test",0L, 0L);
        Assert.assertTrue(record.getExtension().isEmpty());
    }

    @Test
    public void ExtensionExists() {
        var record = new FileHistoryRecord("test.abc", 0L, 0L);
        Assert.assertTrue(record.getExtension().isPresent());
        Assert.assertEquals("abc", record.getExtension().get());
    }
}
